import IMovie from '../interfaces/IMovie';
import MovieDto from '../dto/movieDto';
import API from './API';

class FavoriteServices {
    async getFavorites(): Promise<IMovie[] | undefined> {
        try {
            const moviesIDs: number[] = JSON.parse(
                localStorage.getItem('likedMovies') || '{}'
            );

            const likedMovies: IMovie[] = [];

            if (moviesIDs.length) {
                await Promise.all(
                    moviesIDs.map(async (e) => {
                        likedMovies.push(
                            await new MovieDto(await API.GETmovieinfo(e))
                        );
                    })
                );

                return likedMovies;
            } else {
                return [];
            }
        } catch (e) {
            console.log(e);
        }
    }

    async addLike(id: number): Promise<void> {
        try {
            let likedMovies: number[] = JSON.parse(
                localStorage.getItem('likedMovies') || '{}'
            );

            if (likedMovies.length) {
                likedMovies.push(id);
            } else {
                likedMovies = [id];
            }

            localStorage.setItem('likedMovies', JSON.stringify(likedMovies));
        } catch (e) {
            console.log(e);
        }
    }

    async removeLike(id: number): Promise<void> {
        try {
            let likedMovies: number[] = JSON.parse(
                localStorage.getItem('likedMovies') || '{}'
            );

            likedMovies = likedMovies.filter((e) => e !== id);

            localStorage.setItem('likedMovies', JSON.stringify(likedMovies));
        } catch (e) {
            console.log(e);
        }
    }
}

export default new FavoriteServices();
