import IMovie from '../interfaces/IMovie';

const API_TOKEN: string = 'b6b96375652aa119c2720e99f0fa465f';
const API_URL: string = 'https://api.themoviedb.org/3';

class API {
    async GETpopular() {
        try {
            const response: Response = await fetch(
                `${API_URL}/movie/popular?api_key=${API_TOKEN}`
            );

            const movies: any = await response.json();

            return movies.results;
        } catch (e) {
            console.log(e);
        }
    }

    async GETtoprated() {
        try {
            const response: Response = await fetch(
                `${API_URL}/movie/top_rated?api_key=${API_TOKEN}`
            );

            const movies: any = await response.json();

            return movies.results;
        } catch (e) {
            console.log(e);
        }
    }

    async GETupcoming() {
        try {
            const response: Response = await fetch(
                `${API_URL}/movie/upcoming?api_key=${API_TOKEN}`
            );

            const movies: any = await response.json();

            return movies.results;
        } catch (e) {
            console.log(e);
        }
    }

    async GETsearch(query: string) {
        try {
            const response: Response = await fetch(
                `${API_URL}/search/movie?api_key=${API_TOKEN}&query=${query}`
            );

            const movies: any = await response.json();

            return movies.results;
        } catch (e) {
            console.log(e);
        }
    }

    async GETmovieinfo(id: number) {
        try {
            const response: Response = await fetch(
                `${API_URL}/movie/${id}?api_key=${API_TOKEN}`
            );

            const movies = await response.json();

            return movies;
        } catch (e) {
            console.log(e);
        }
    }
}

export default new API();
