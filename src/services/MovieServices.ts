import API from './API';
import IMovie from '../interfaces/IMovie';
import MovieDto from '../dto/movieDto';

class FilmServices {
    async getPopular(): Promise<IMovie[]> {
        const movies: IMovie[] = await API.GETpopular();

        return this.mapping(movies);
    }

    async getTopRated(): Promise<IMovie[]> {
        const movies: IMovie[] = await API.GETtoprated();

        return this.mapping(movies);
    }

    async getUpComing(): Promise<IMovie[]> {
        const movies: IMovie[] = await API.GETupcoming();

        return this.mapping(movies);
    }

    async searching(query: string): Promise<IMovie[]> {
        const movies: IMovie[] = await API.GETsearch(query);

        return this.mapping(movies);
    }

    async mapping(arr: IMovie[]): Promise<IMovie[]> {
        const movies: IMovie[] = [];

        await Promise.all(
            arr.map(async (e) => {
                movies.push(await new MovieDto(e));
            })
        );

        return movies;
    }
}

export default new FilmServices();
