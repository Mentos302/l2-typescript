import MovieDto from '../dto/movieDto';
import IMovie from '../interfaces/IMovie';
import API from './API';

class RandomMovieServices {
    async getRandomMovie(): Promise<IMovie> {
        const movies = await API.GETpopular();

        const movie: MovieDto = await new MovieDto(
            movies[Math.floor(Math.random() * movies.length) - 1]
        );

        return movie;
    }
}

export default new RandomMovieServices();
