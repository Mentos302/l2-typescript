import getComponents from './components';

export async function render(): Promise<void> {
    getComponents();
}
