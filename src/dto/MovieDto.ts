import IMovie from '../interfaces/IMovie';

export default class MovieDto {
    id;
    poster_path;
    overview;
    release_date;
    backdrop_path;
    title;

    constructor(model: IMovie) {
        this.id = model.id;
        this.poster_path = model.poster_path;
        this.backdrop_path = model.backdrop_path;
        this.title = model.title;
        this.overview = model.overview;
        this.release_date = model.release_date;
    }
}
