interface IMovie {
    id: number;
    poster_path: string;
    overview: string;
    release_date: string;
    backdrop_path: string;
    title: string;
}

export default IMovie;
