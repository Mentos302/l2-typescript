import IMovie from '../interfaces/IMovie';
import RandomMovieServices from '../services/RandomMovieServices';

class RandomFilmController {
    async renderRandomMovie(component: HTMLElement): Promise<void> {
        const movie: IMovie = await RandomMovieServices.getRandomMovie();

        component.style.background = `url(https://image.tmdb.org/t/p/original${movie.backdrop_path}) center center / cover`;

        const title: HTMLElement | null =
            document.getElementById('random-movie-name');
        if (title) {
            title.innerHTML = movie.title;
        }

        const overview: HTMLElement | null = document.getElementById(
            'random-movie-description'
        );
        if (overview) {
            overview.innerHTML = movie.overview;
        }
    }
}

export default new RandomFilmController();
