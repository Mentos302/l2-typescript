import IMovie from '../interfaces/IMovie';
import FavoriteServices from '../services/FavoriteServices';

class FavoritesController {
    async likeToogle(id: number, el: HTMLElement): Promise<void> {
        if (el.parentElement?.getAttribute('fill') == 'red') {
            el.parentElement?.setAttribute('fill', '#ff000078');
            await FavoriteServices.removeLike(id);

            el.classList.remove('liked');
        } else {
            el.parentElement?.setAttribute('fill', 'red');
            await FavoriteServices.addLike(id);

            el.classList.add('liked');
        }
    }

    async renderFavorite(component: HTMLElement): Promise<void> {
        const likedMovies: number[] = JSON.parse(
            localStorage.getItem('likedMovies') || '{}'
        );

        const favorites: IMovie[] | undefined =
            await FavoriteServices.getFavorites();

        if (favorites) {
            favorites.map((e) => {
                if (e.poster_path) {
                    const likeEl = (id: number) => {
                        const isLiked = (): boolean => {
                            if (likedMovies.length) {
                                return likedMovies.some((e) => e == id);
                            } else {
                                return false;
                            }
                        };

                        if (isLiked()) {
                            return `<svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="red" width="50" height="50" class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22">
                            <path class="liked" fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"></path>
                            </svg>`;
                        } else {
                            return `<svg xmlns="http://www.w3.org/2000/svg"
                            stroke="red"
                            fill="#ff000078"
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                fill-rule="evenodd"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>`;
                        }
                    };

                    const item: HTMLDivElement = document.createElement('div');

                    item.innerHTML = `
                    <div class="card shadow-sm">
                    <img
                        src="https://image.tmdb.org/t/p/original${
                            e.poster_path
                        }"
                    />
                    ${likeEl(e.id)}
                    <div class="card-body">
                        <p class="card-text truncate">
                            ${e.overview}
                        </p>
                        <div
                            class="
                                d-flex
                                justify-content-between
                                align-items-center
                            "
                        >
                            <small class="text-muted">${e.release_date}</small>
                        </div>
                    </div>
                </div>`;

                    item.classList.add('col-12', 'p-2');

                    item.addEventListener('click', (event) =>
                        this.likeToogle(e.id, event.target as HTMLElement)
                    );

                    component.appendChild(item);
                }
            });
        }
    }
}
export default new FavoritesController();
