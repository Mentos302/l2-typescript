import FilmsContainer from '../components/FilmsContainer';

class SortingController {
    async changeSorting(sorting: 'popular' | 'upcoming' | 'top_rated') {
        FilmsContainer(
            document.getElementById('film-container') as HTMLElement,
            sorting
        );
    }
}

export default new SortingController();
