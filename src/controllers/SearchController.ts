import IMovie from '../interfaces/IMovie';
import MoviesController from './MoviesController';
import MovieServices from '../services/MovieServices';

class SearchController {
    async renderFounded(query: string): Promise<void> {
        const container: HTMLElement =
            document.getElementById('film-container')!;
        container.innerHTML = ``;

        const movies: IMovie[] = await MovieServices.searching(query);

        MoviesController.movieRender(container, movies);
    }
}

export default new SearchController();
