import RandomFilmController from '../controllers/RandomFilmController';

export default (component: HTMLElement): void => {
    RandomFilmController.renderRandomMovie(component);
};
