import FavoriteController from '../controllers/FavoriteController';

export default (component: HTMLElement): void => {
    component.innerHTML = ``;

    FavoriteController.renderFavorite(component);
};
