import MoviesController from '../controllers/MoviesController';

export default (
    component: HTMLElement,
    sorting: 'popular' | 'upcoming' | 'top_rated' = 'popular'
): void => {
    component.innerHTML = ``;

    switch (sorting) {
        case 'popular':
            MoviesController.renderPopular(component);
            break;
        case 'upcoming':
            MoviesController.renderUpComing(component);
            break;
        case 'top_rated':
            MoviesController.renderTopRated(component);
            break;
    }
};
