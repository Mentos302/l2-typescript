import SortingController from '../controllers/SortingController';

export default (component: HTMLElement): void => {
    const btns: any[] = Array.prototype.slice.call(component.children);

    btns.map((e) => {
        e.addEventListener('click', () =>
            SortingController.changeSorting(e.id)
        );
    });
};
