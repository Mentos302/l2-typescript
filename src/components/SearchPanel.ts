import SearchController from '../controllers/SearchController';

export default (component: HTMLFormElement): void => {
    const el: any[] = Array.prototype.slice.call(component.children);

    el[1].onclick = () => {
        if (el[0].value) {
            SearchController.renderFounded(el[0].value);
            el[0].value = '';
        } else {
            alert('Please enter the movie title.');
        }
    };
};
