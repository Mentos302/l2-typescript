import RandomMovie from './RandomMovie';
import SortingButtons from './SortingButtons';
import SearchPanel from './SearchPanel';
import FilmsContainer from './FilmsContainer';
import FavoriteFilms from './FavoriteFilms';

export default () => {
    RandomMovie(document.getElementById('random-movie') as HTMLElement);
    SortingButtons(document.getElementById('button-wrapper') as HTMLElement);
    SearchPanel(document.getElementById('searching_form') as HTMLFormElement);
    FilmsContainer(document.getElementById('film-container') as HTMLElement);
    FavoriteFilms(document.getElementById('favorite-movies') as HTMLElement);
};
